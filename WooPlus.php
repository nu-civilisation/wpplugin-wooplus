<?php
/**
 * @wordpress-plugin
 * Plugin Name:       WooPlus
 * Plugin URI:        https://www.nu-civilisation.org/
 * Description:       Adds really essential functionality to WooCommerce. -- Amasing!
 * Version:           1.0.0
 * Author:            NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation
 * Author URI: 		  https://www.nu-civilisation.org
 * License:           GPL-3.0
 * License URI:       https://www.gnu.org/licenses/gpl-3.0.en.html
 * Text Domain:       wooplus
 */

/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

require_once(plugin_dir_path(__FILE__) . 'mvc/models/WooplusAccessKeys.php');
require_once(plugin_dir_path(__FILE__) . 'mvc/models/WooplusDefinedKeys.php');
require_once(plugin_dir_path(__FILE__) . 'mvc/models/WooplusTranslatedKeys.php');
require_once(plugin_dir_path(__FILE__) . 'mvc/models/WooplusAccessRules.php');
require_once(plugin_dir_path(__FILE__) . 'mvc/models/WooplusSubscriptions.php');
require_once(plugin_dir_path(__FILE__) . 'mvc/views/AdminNotices.php');
require_once(plugin_dir_path(__FILE__) . 'mvc/views/AdminPageAccessManagement.php');
require_once(plugin_dir_path(__FILE__) . 'mvc/views/HtmlMetaboxAccessLabel.php');
require_once(plugin_dir_path(__FILE__) . 'mvc/views/HtmlAccessShortcodes.php');
require_once(plugin_dir_path(__FILE__) . 'mvc/views/WooAccessEditor.php');
require_once(plugin_dir_path(__FILE__) . 'mvc/controllers/WooAccessHandling.php');

class WooPlusPlugin {

    // Custom WP Events:
    // - 'wooplus_accesskey_added': $accessKeyCode
    //   ...In the WooplusAccessKeys model in the add() function.
    // - 'wooplus_accesskey_removed': $accessKeyCode
    //   ...In the WooplusAccessKeys model in the remove() function.

    private $adminNotices;
    // ...The admin-notices enclosed in the WooplusAdminNotices class.
    private $wooAdminPageAccessManagement;
    // ...The admin page for configuring the page access rules.
    private $wooPageMetaboxAccessLabel;
    // ...The page metabox editor for the access-labels enclosed in the WooplusPageAccessLabelMetabox class.
    private $wooHtmlAccessShortcodes;
    // ...The page access shortcodes enclosed in the WooplusHtmlAccessShortcodes class.
    private $wooAccessEditor;
    // ...The WooCommerce access editor enclosed in the WooplusAccessEditor class.
    private $wooAccessHandling;
    // ...The WooCommerce access handling enclosed in the WooplusAccessHandling class.

    public function __construct() {
        $this->adminNotices = new WooplusAdminNotices();
        // ...Encapsule the admin notices in an own class.
        $this->wooAdminPageAccessManagement = new WooplusAdminPageAccessManagement();
        // ...Encapsule the admin page for configuring the page access rules.
        $this->wooPageMetaboxAccessLabel = new WooplusPageMetaboxAccessLabel();
        // ...Encapsule the page metabox editor for editing the access-labels in an own class.
        $this->wooHtmlAccessShortcodes = new WooplusHtmlAccessShortcodes();
        // ...Encapsule the page access shortcodes in an own class.
        $this->wooAccessEditor = new WooplusAccessEditor();
        // ...Encapsule the WooCommerce subscription editor in an own class.
        $this->wooAccessHandling = new WooplusAccessHandling();
        // ...Encapsule the WooCommerce access handling in an own class.
    
        add_action('admin_init', array($this, 'ensureDependencies'));
        // ...Ensure, that the required plugin (i.e. WooCommerce) is active.
        add_action('init', array($this, 'registerAccessLabel'));
        // ...Register the custom taxonomy for pages and the like to be able to restrict access.
        add_action('wooplus_cron', array($this, 'runCron'));
        // ...Run the cron function watching out for expired subscriptions.
        // ...It is important to have the add_action function call exactly on this place!

        add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($this, 'linkAdminAccessManagement'), 10, 1);
        // ...Add the WooPlus access management page link to the plugins list.
    }

    public function activate() {
        WooplusAccessKeys::exists();
        // ...Creates or modifies -- if necessary -- the GENERAL 'wooplus_accesskeys' entity table.
        WooplusDefinedKeys::exists();
        // ...Creates or modifies -- if necessary -- the GENERAL 'wooplus_definedkeys' entity table.
        WooplusTranslatedKeys::exists();
        // ...Creates or modifies -- if necessary -- the GENERAL 'wooplus_translatedkeys' entity table.
        WooplusAccessRules::exists();
        // ...Creates or modifies -- if necessary -- the SPECIFIC 'wooplus_accessrules' entity table.
        WooplusSubscriptions::exists();
        // ...Creates or modifies -- if necessary -- the SPECIFI 'wooplus_subscriptions' entity table.

        add_action('wooplus_cron', array($this, 'runCron'));
        // ...Run the cron function watching out for expired subscriptions.
        // ...It is important to have the add_action function call exactly on this place!
        if(!wp_next_scheduled('wooplus_cron')) {
            wp_schedule_event(time(), 'hourly', 'wooplus_cron');
            // ...Enregister the subscription watching cron activity from WP-Cron.
        }

        $this->ensureTranslations();
    }

    public function deactivate() {
        $timestamp = wp_next_scheduled('wooplus_cron');
        if($timestamp) {
            wp_unschedule_event($timestamp, 'wooplus_cron');
            // ...Unregister the subscription watching cron activity from WP-Cron.
        }
    }

    public function ensureDependencies() {
        if(!is_plugin_active('woocommerce/woocommerce.php')) {
            $notice = array(
                'id'                  => 'woocommerce-not-active',
                'type'                => 'error',
                'notice'              => __('WooPlus requires the WooCommerce WP-plugin to be installed and activated.', 'wooplus'),
                'dismissable_forever' => false
            );
            
            $this->adminNotices->addNotice($notice);
        }
    }

    public function registerAccessLabel() {
        register_taxonomy
        ( 'wooplus_label'
        // ...The taxonomy name.
        , 'page'
        // ...The post type to be associated with.
        , array
            ( 'label' => __('WooPlus Access Label', 'wooplus')
            , 'hierarchical' => false,
            // ...Deliberately this custom taxonomy type is not hierarchial, but only flat!
            )
        );

        register_taxonomy_for_object_type('wooplus_label', 'page');
    }

    public function linkAdminAccessManagement($links) {
        $toolsLink = '<a href="options-general.php?page=wooplusAccessManagement">' . __('Access Mnanagement', 'wooplus') . '</a>';
        array_push($links, $toolsLink);
        
        return $links;
    }

    public function runCron() {
        WooplusAccessHandling::watchSubscriptions();
        // ...Watches the subscriptions, which are expired and invalidates them.
    }

    private function ensureTranslations() {
        $i18nFolderPath        = plugin_dir_path(__FILE__) . 'i18n/';
        $destinationFolderPath = plugin_dir_path(__FILE__) . '../../languages/plugins/';
        $moFiles = glob($i18nFolderPath . 'wooplus-*.mo');
        foreach ($moFiles as $moFile) {
            $moBasename = basename($moFile);
            copy($moFile, $destinationFolderPath . $moBasename);
        }
    }
}

$wooplusPlugin = new WooPlusPlugin();
register_activation_hook(__FILE__, array($wooplusPlugin, 'activate'));
register_deactivation_hook(__FILE__, array($wooplusPlugin, 'deactivate'));
?>
