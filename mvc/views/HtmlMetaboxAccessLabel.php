<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

class WooplusPageMetaboxAccessLabel {
    
    public function __construct() {
        add_action('add_meta_boxes', array($this, 'addMetaboxAccessLabel'));
        add_action('save_post', array($this, 'saveMetaboxAccessLabel'));
        add_action('admin_enqueue_scripts', array($this, 'enqueueMetaboxAccessLabelScripts'));
    }

    public function addMetaboxAccessLabel() {
        add_meta_box
        ( 'wooplusPageAccessLabelMetabox'
        // ...This is a unique ID for the meta box.
        , __('Access Labels', 'wooplus')
        // ...The title or label that appears on the meta box in the admin panel.
        , array($this, 'displayMetaboxAccessLabel')
        // ...The callback function rendering the HTML and controls inside the meta box.
        , 'page'
        // ...Specifies the post-type(s) to which the meta box should be added.
        , 'side'
        // ...The position of the meta box: e.g., 'normal', 'side', 'advanced'.
        , 'default'
        // ...The priority of the meta box: e.g., 'high', 'core', 'default', 'low'.
        );
    }

    public function displayMetaboxAccessLabel($post) {
        $theAccessLabel = get_post_meta($post->ID, '_access_label', true);
        $accessLabels = get_terms('wooplus_label', array('hide_empty' => false));
        echo '<style>';
        echo '.selectbox select{width:100%;}';
        echo '</style>';
        echo '<table width="100%">';
        echo '<tr>';
        echo '<td width="80%">';
        echo '<div class="selectbox">';
        echo '<select name="access-label" id="access-label">';
        echo '<option value=""> </option>';
        foreach ($accessLabels as $accessLabel) {
            $selected = ($theAccessLabel == $accessLabel->term_id) ? ' selected' : '';
            echo '<option value="' . $accessLabel->term_id . '"' . $selected . '>' . $accessLabel->name . '</option>';
        }
        echo '</select>';
        echo '</div>';
        echo '</td>';
        echo '<td width="20%"><a href="#" class="remove-label">x</a></td>';
        echo '</tr>';
        echo '</table>';
        echo '<script>';
        echo "jQuery(document).ready(function($) {";
        echo "  $('.remove-label').on('click', function(e) {";
        echo "    e.preventDefault();";
        echo "    $('#access-label').val('');";
        echo "  });";
        echo "});";
        echo '</script>';
    }

    public function saveMetaboxAccessLabel($postId) {
        if(wp_is_post_revision($postId) || wp_is_post_autosave($postId) || 'page' !== get_post_type($postId)) {
            return;
            // ...Check if this is a page and not an autosave or post revision.
        }

        if ($postId && isset($_POST['access-label'])) {
            update_post_meta($postId, '_access_label', $_POST['access-label']);
        }
    }

    public function enqueueMetaboxAccessLabelScripts($hook) {
        if ('post.php' === $hook || 'post-new.php' === $hook) {
            wp_enqueue_script('postbox');
            wp_enqueue_style('postbox');
        }
    }
}
?>
