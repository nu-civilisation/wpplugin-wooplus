<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

require_once(plugin_dir_path(__FILE__) . '../models/WooplusDefinedKeys.php');
require_once(plugin_dir_path(__FILE__) . '../models/WooplusTranslatedKeys.php');

class WooplusAccessEditor {

    public function __construct() {
        add_filter('woocommerce_product_data_tabs', array($this, 'addAccessTab'));
        // ...Add the access tab to the product tabs.
        add_action('woocommerce_product_data_panels', array($this, 'addAccessFields'));
        // ...Add the access fields to the subscriptino product tab.
        add_action('woocommerce_process_product_meta', array($this, 'saveAccessFields'), 10, 2);
        // ...Save the access fields in the product editor.
        add_action('admin_footer', array($this, 'enqueueAccessFieldsScript'));
        // ...Add the JavaScript file, which toggles the visibility of the custom fields.
    }

    public function addAccessTab($tabs) {
        $tabs['access_tab'] = array(
            'label'       => __('Access', 'wooplus'),
            'target'      => 'access_product',
            'class'       => array('show_if_simple', 'show_if_variable'),
            // ...Only show the subscription tab, when the product is a simple or variable product.
            'priority'    => 12,
            // ...Adjusts the priority to change the position of the tab.
        );
        return $tabs;
   }

    public function addAccessFields() {
        global $post;

        $currentLanguageCode = get_locale();
        $currentSite = get_current_blog_id();
        $optionKeys = array();
        $optionKeys[] = ' ';
        $definedKeys = WooplusDefinedKeys::list();
        foreach($definedKeys as $definedKey) {
            $definedKeyOnlySite = $definedKey->onlySite;
            if((!isset($definedKeyOnlySite)) || ($definedKeyOnlySite == $currentSite)) {
                $keyCode = $definedKey->key_code;
                $keyName = $definedKey->key_name;
                if($currentLanguageCode <> 'en_US') {
                    $translatedKey = WooplusTranslatedKeys::get($keyCode, $currentLanguageCode);
                    $keyName = (isset($translatedKey->key_name)) ? $translatedKey->key_name : $keyName;
                }
                $optionKeys[$keyCode] = $keyName;
            }
        }
        echo '<div id="access_product" class="panel woocommerce_options_panel">';
        echo '<h2>'. __('Required Access Key', 'wooplus') . '</h2>';
        woocommerce_wp_checkbox(array(
            'id'          => 'access_needs_login',
            'value'       => get_post_meta($post->ID, 'access_needs_login', true),
            'label'       => __('Needs login', 'wooplus') . '?',
            'desc_tip'    => true,
            // ...If set to true, then the description will be displayed as a tip.
            'description' => __('The user needs to be logged in to see the product.', 'wooplus'),
        ));
        woocommerce_wp_select(array(
        // ...Deliberately use the WooCommerce standard functions instead of raw HTML.
            'id'                => 'access_required_key',
            'value'             => get_post_meta($post->ID, 'access_required_key', true),
            'label'             => __('Required Access-Key', 'wooplus'),
            'options'           => $optionKeys,
            'desc_tip'          => true,
            // ...If set to true, then the description will be displayed as a tip.
            'description'       => __('The required access key specifies the visibility of the product: this access key is needed to show the product.', 'wooplus')
        ));
        echo '<hr>';

        echo '<h2>'. __('Subscription', 'wooplus') . '</h2>';
        woocommerce_wp_checkbox(array(
            'id'          => 'is_subscription',
            'value'       => get_post_meta($post->ID, 'is_subscription', true),
            'label'       => __('Subscription', 'wooplus') . '?',
            'desc_tip'    => true,
            // ...If set to true, then the description will be displayed as a tip.
            'description' => __('Determines, if this product is a subscription.', 'wooplus'),
        ));

        echo '<div id="subscription_product_fields">';
        woocommerce_wp_text_input(array(
        // ...Deliberately use the WooCommerce standard functions instead of raw HTML.
            'id'                => 'subscription_period_length',
            'value'             => get_post_meta($post->ID, 'subscription_period_length', true),
            'label'             => __('Subscription period length', 'wooplus'),
            'type'              => 'number',
            'custom_attributes' => array(
                'min'  => '1',
                'max'  => '999',
            ),
            'desc_tip'          => true,
            // ...If set to true, then the description will be displayed as a tip.
            'description'       => __('The decimal value of the subscription length per period unit.', 'wooplus'),
        ));
        woocommerce_wp_select(array(
        // ...Deliberately use the WooCommerce standard functions instead of raw HTML.
            'id'                => 'subscription_period_unit',
            'value'             => get_post_meta($post->ID, 'subscription_period_unit', true),
            'label'             => __('Subscription period unit', 'wooplus'),
            'options'           => array(
                'days'              => __('days', 'wooplus'),
                'weeks'             => __('weeks', 'wooplus'),
                'months'            => __('months', 'wooplus'),
                'years'             => __('years', 'wooplus'),
            ),
            'desc_tip'          => true,
            // ...If set to true, then the description will be displayed as a tip.
            'description'       => __('The subscription period unit for the provided subscription length.', 'wooplus')
        ));
        echo '</div>';
        // ...Be sure to close the options group div.
        echo '<hr>';

        $gainingKeysString = get_post_meta($post->ID, 'access_gaining_keys', true);
        $gainingKeyCodes = (strlen($gainingKeysString) > 0) ?  explode(',', $gainingKeysString) : array();
        $allPairs = array();
        $allPairs[' '] = ' ';
        $definedKeys = WooplusDefinedKeys::list();
        foreach($definedKeys as $definedKey) {
            $definedKeyOnlySite = $definedKey->onlySite;
            if((!isset($definedKeyOnlySite)) || ($definedKeyOnlySite == $currentSite)) {
                $keyCode = $definedKey->key_code;
                $keyName = $definedKey->key_name;
                if($currentLanguageCode <> 'en_US') {
                    $translatedKey = WooplusTranslatedKeys::get($keyCode, $currentLanguageCode);
                    $keyName = (isset($translatedKey->key_name)) ? $translatedKey->key_name : $keyName;
                }
                $allPairs[$keyCode] = $keyName;
            }
        }
        $optionKeys = array();
        foreach($allPairs as $keyCode => $keyName) {
            if(!in_array($keyCode, $gainingKeyCodes)) {
                $optionKeys[$keyCode] = $keyName;
            }
        }
        echo '<h2>'. __('Gaining Access Keys', 'wooplus') . '</h2>';
        woocommerce_wp_select(array(
        // ...Deliberately use the WooCommerce standard functions instead of raw HTML.
            'id'                => 'access_gaining_key',
            'value'             => get_post_meta($post->ID, 'access_gaining_key', true),
            'label'             => __('New Gaining Access-Key', 'wooplus'),
            'options'           => $optionKeys,
            'desc_tip'          => true,
            // ...If set to true, then the description will be displayed as a tip.
            'description'       => __('The gaining access-keys specify which keys are gained with obtaining the product.', 'wooplus')
        ));
        $i = 0;
        foreach($gainingKeyCodes as $gainingKeyCode) {
            $id = 'access_gaining_key_' . sprintf('%02d', $i);
            // ...Two digits should be enough: It is not estimated, that a product is gaining more than a 100 keys. ;-)
            woocommerce_wp_checkbox(array(
                'id'          => $id,
                'value'       => 'yes',
                // ...Deliberately tick the checkbox, since the access key wants to be kept per default.
                'label'       => $allPairs[$gainingKeyCode],
                // ...Displays the possibly translated key name. 
                'desc_tip'    => true,
                // ...If set to true, then the description will be displayed as a tip.
                'description' => __('Deselecting this checkbox removes the access key.', 'wooplus'),
            ));

            $i++;
        }

        echo '</div>';
        // ...Be sure to close the access-product div.
    }

    public function saveAccessFields($postId, $post) {
        $needsLogin = isset($_POST['access_needs_login']) ? 'yes' : 'no';
        update_post_meta($postId, 'access_needs_login', $needsLogin);
		if(isset($_POST['access_required_key'])) {
	        update_post_meta($postId, 'access_required_key', sanitize_text_field($_POST['access_required_key']));
		}

        $isSubscription = isset($_POST['is_subscription']) ? 'yes' : 'no';
        update_post_meta($postId, 'is_subscription', $isSubscription);

		if(isset($_POST['subscription_period_length'])) {
	        update_post_meta($postId, 'subscription_period_length', sanitize_text_field($_POST['subscription_period_length']));
		}
		if(isset($_POST['subscription_period_unit'])) {
	        update_post_meta($postId, 'subscription_period_unit', sanitize_text_field($_POST['subscription_period_unit']));
		}

        $gainingKeys = array();
		if(isset($_POST['access_gaining_key'])) {
            $gainingKeyCode = sanitize_text_field($_POST['access_gaining_key']);
            if(!empty($gainingKeyCode)) {
                $gainingKey = WooplusDefinedKeys::get($gainingKeyCode);
                $gainingKeyName = $gainingKey->key_name;
                $gainingKeys[$gainingKeyCode] = $gainingKeyName;
            }
		}
        $gainingKeysString = get_post_meta($post->ID, 'access_gaining_keys', true);
        $gainingKeyCodes = (strlen($gainingKeysString) > 0) ?  explode(',', $gainingKeysString) : array();
        $n = count($gainingKeyCodes);
        for($i = 0; $i<$n; $i++) {
            $postAttributeName = 'access_gaining_key_' . sprintf('%02d', $i);
            if(isset($_POST[$postAttributeName])) {
                $gainingKeyCode = $gainingKeyCodes[$i];
                $gainingKey = WooplusDefinedKeys::get($gainingKeyCode);
                $gainingKeyName = $gainingKey->key_name;
                $gainingKeys[$gainingKeyCode] = $gainingKeyName;
                }
        }
        asort($gainingKeys);
        // ...Sort the gaining keys by the access key name ascending.
        $gainingKeysString = implode(',', array_keys($gainingKeys));
        if(empty($gainingKeysString)) {
            delete_post_meta($postId, 'access_gaining_keys');
        }
        else {
            update_post_meta($postId, 'access_gaining_keys', $gainingKeysString);
        }
    }

    public function enqueueAccessFieldsScript() {
        wc_enqueue_js('
            jQuery(document).ready(function($) {
                // Check the initial state of the checkbox:
                if (!$("#is_subscription").is(":checked")) {
                     $("#subscription_product_fields").hide();
                }
               
                // Toggle the custom fields on checkbox change:
                $("#is_subscription").change(function() {
                    if ($(this).is(":checked")) {
                        $("#subscription_product_fields").show();
                        $("input[name=\'access_needs_login\']").prop(\'checked\', true);
                    } else {
                        $("#subscription_product_fields").hide();
                    }
                });
            });
        ');
    }
}
?>