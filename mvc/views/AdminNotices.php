<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

class WooplusAdminNotices {

    private $notices;
    // ...Holds the array of admin-notices to be displayed.
    private $noticesOptions;
    // ...The plugin's admin options.
    
    public function __construct() {
        add_action('admin_notices', array($this, 'displayNotices'));
        // ...Hook for displaying the admin-notices.
        add_action('wp_ajax_wooplus_dismiss_admin_notices', array($this, 'dismissNotice'));
        // ...Catch the dismiss notice action and add it to the dismissed notices array.
        
        $this->noticesOptions = apply_filters('get_plugin_options', get_option('wooplusAdminNoticesOptions', array()));
        // ...Get the plugin's admin options.
    }
    
    public function displayNotices() {
        if($this->notices) {
            foreach($this->notices as $notice) {
                $displayNotice = array("notice notice-{$notice['type']}", "is-dismissible");
                // ...Display the notice only, if it wasn't dismissed in the past.
                
                $id = $notice['id'];
                if(!$notice['dismissable_forever'] || (!isset( $this->noticesOptions['dismiss_notices'][$id]) || !$this->noticesOptions['dismiss_notices'][$id])) {
                    if($notice['dismissable_forever']) {
                        $displayNotice[] = 'wooplusAdminNoticeDismissForever';
                    }
                    
                    echo "<div id='{$notice['id']}' class='".implode(' ', $displayNotice)."'><p>{$notice['notice']}</p></div>";
                }
            }
        }
    }
    
    public function addNotice($notice = "") {
        if($notice) {
            $this->notices[] = array(
                'id'     => $notice['id'],
                'notice' => $notice['notice'],
                'type'   => isset($notice['type']) ? $notice['type'] : 'info',
                // ...Types can be: "error", "warning", "success", "info".
                'dismissable_forever' => isset($notice['dismissable_forever']) ? $notice['dismissable_forever'] : false
            );
        }
    }
    
    public function dismissNotice() {
        $id = isset($_POST['id']) ? sanitize_text_field($_POST['id']) : '';
        
        if($id) {
            $this->noticesOptions['dismiss_notices'][$id] = true;
            update_option('wooplusAdminNoticesOptions' , $this->noticesOptions);
            // ...Save the dismissed notice to the plugin options.
        }
        
        die('updated');
    }
}
?>