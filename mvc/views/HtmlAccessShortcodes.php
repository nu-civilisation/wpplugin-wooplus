<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

require_once(plugin_dir_path(__FILE__) . '../models/WooplusDefinedKeys.php');
require_once(plugin_dir_path(__FILE__) . '../models/WooplusTranslatedKeys.php');
require_once(plugin_dir_path(__FILE__) . '../models/WooplusAccessKeys.php');

class WooplusHtmlAccessShortcodes {

    public function __construct() {
        add_shortcode('if', array($this, 'renderShortcodeIf'));
        // ...Rendering the shortcode having the name "if".
    }

    public function renderShortcodeIf($atts = [], $content = '') {
        $atts   = shortcode_atts(array('c' => ''), $atts);
        $ifC    = $atts['c'];

        if($ifC == 'logged in') {
            if(is_user_logged_in()) {
                $content = $this->extractByTag("then", $content);
            }
            else {
                $content = $this->extractByTag("else", $content);
            }
        }
        else if(strpos($ifC, 'key = ') === 0) {
            $accessKeyName = substr($ifC, 6);
            $currentLanguageCode = get_locale();
            $userId = get_current_user_id();
            $translatedKey = WooplusTranslatedKeys::getByName($accessKeyName, $currentLanguageCode);
            $keyCode = (isset($translatedKey->key_code)) ? $translatedKey->key_code : '';
            if(empty($keyCode)) {
                $definedKey = WooplusDefinedKeys::getByName($accessKeyName);
                $keyCode = (isset($definedKey->key_code)) ? $definedKey->key_code : '';
            }
            if(WooplusAccessKeys::have($keyCode, $userId)) {
                $content = $this->extractByTag("then", $content);
            }
            else {
                $content = $this->extractByTag("else", $content);
            }
        }
        $content = do_shortcode($content);
        // ...Be sure to run the shortcode parser recursively to parse shortcodes in the content.

        return $content;
        // ...important: always return!
    }

    private function extractByTag($tagName, $content) {
        $startTag = '[' . $tagName . ']';
        $endTag   = '[/' . $tagName . ']';
        $result   = '';
        $stack    = [];
        $index    = 0;
    
        while ($index < strlen($content)) {
            $nextStartTagPos = strpos($content, $startTag, $index);
            $nextEndTagPos   = strpos($content, $endTag, $index);
    
            if ($nextStartTagPos === false && $nextEndTagPos === false) {
                // No more tags found, exit:
                break;
            }
    
            if ($nextStartTagPos !== false && ($nextStartTagPos < $nextEndTagPos || $nextEndTagPos === false)) {
                // Found a start tag
                $stack[] = $nextStartTagPos;
                $index = $nextStartTagPos + strlen($startTag);
            } elseif ($nextEndTagPos !== false) {
                // Found an end tag
                if (count($stack) > 0) {
                    // Match the end tag with the last encountered start tag
                    $startPos = array_pop($stack);
                    if (empty($stack)) {
                        // Extract the content between matching tags (excluding the end tag)
                        $result .= substr($content, $startPos + strlen($startTag), $nextEndTagPos - ($startPos + strlen($startTag)));
                    }
                    $index = $nextEndTagPos + strlen($endTag);
                } else {
                    // Unmatched end tag, ignore and continue
                    $index = $nextEndTagPos + strlen($endTag);
                }
            } else {
                // Unmatched start tag, ignore and continue
                $index = $nextStartTagPos + strlen($startTag);
            }
        }
    
        return $result;
    }
}
?>