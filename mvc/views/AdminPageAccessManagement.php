<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

require_once(plugin_dir_path(__FILE__) . '../models/WooplusDefinedKeys.php');
require_once(plugin_dir_path(__FILE__) . '../models/WooplusTranslatedKeys.php');
require_once(plugin_dir_path(__FILE__) . '../models/WooplusAccessRules.php');

class WooplusAdminPageAccessManagement {

    public function __construct() {
        add_action('admin_menu', array($this, 'addSubmenuPage'));
        
        add_action('admin_post_toWooplusAccessKeyEdit', array($this, 'toAccessKeyEdit'));
        add_action('admin_post_toWooplusAccessKeyTranslate', array($this, 'toAccessKeyTranslate'));
        add_action('admin_post_addWooplusAccessKey', array($this, 'addAccessKey'));
        add_action('admin_post_editWooplusAccessKey', array($this, 'editAccessKey'));
        add_action('admin_post_translateWooplusAccessKey', array($this, 'translateAccessKey'));
        add_action('admin_post_removeWooplusAccessKey', array($this, 'removeAccessKey'));

        add_action('admin_post_addWooplusAccessRule', array($this, 'addAccessRule'));
        add_action('admin_post_removeWooplusAccessRule' , array($this, 'removeAccessRule'));

        add_action('admin_post_addWooplusAccessLabel', array($this, 'addAccessLabel'));
        add_action('admin_post_removeWooplusAccessLabel' , array($this, 'removeAccessLabel'));
    }

    public function addSubmenuPage() {
        add_submenu_page
        ( 'options-general.php'
        // ...The parent menu; here: the "Settings" menu.
        , __('Access Management', 'wooplus')
        // ...The text that will be displayed as the submenu page's title in the admin menu.
        , __('Access Management', 'wooplus')
        // ...The text that will be displayed in the browser's title bar when the submenu page is open.
        , 'manage_options'
        // ...The capability required for a user to access this submenu page.
        , 'wooplusAccessManagement'
        // ...The unique slug.
        , array($this, 'displayAccessManagement')
        // ...The callback function.
        );
        // ...Adds a submenu page to the WordPress admin menu.
    }

    public function displayAccessManagement() {
        echo '<style>'
        . 'table {'
        . '  width:100%;'
        . '} '
        . 'th, td {'
        . '  padding: 5px;'
        . '  text-align: center;'
        . '} '
        . '.highlight-row {'
        . '  background-color: #eee;'
        . '} '
        . 'table#t01 tr:nth-child(even) {'
        . '  background-color: #eee;'
        . '} '
        . 'table#t01 tr:nth-child(odd) {'
        . '  background-color: #fff;'
        . '}'
        . '.text-cell {'
        . '  text-align: left;'
        . '  vertical-align: middle;'
        . '}'
        . '.button-cell {'
        . '  text-align: right;'
        . '  vertical-align: middle;'
        . '}'
        . '.button-primary {'
        . '  padding: 15px 30px;'
        . '  font-size: 110%;'
        . '}'
        . '</style>'
        ;

        $activeTab = isset($_GET['tab']) ? $_GET['tab'] : 'tab1';
        $tab1active = ($activeTab == 'tab1') ? ' nav-tab-active' : '';
        $tab2active = ($activeTab == 'tab2') ? ' nav-tab-active' : '';
        $tab3active = ($activeTab == 'tab3') ? ' nav-tab-active' : '';

        echo '<div class="wrap">';
        echo '<h2>' . __('Access Management', 'wooplus') . '</h2>';
        echo '<nav class="nav-tab-wrapper">';
        echo '<a href="?page=wooplusAccessManagement&tab=tab1" class="nav-tab' . $tab1active . '">' . __('Access Keys', 'wooplus') . '</a>';
        echo '<a href="?page=wooplusAccessManagement&tab=tab2" class="nav-tab' . $tab2active . '">' . __('Page Access Rules', 'wooplus') . '</a>';
        echo '<a href="?page=wooplusAccessManagement&tab=tab3" class="nav-tab' . $tab3active . '">' . __('Page Access Labels', 'wooplus') . '</a>';
        echo '</nav>';

        switch ($activeTab) {
        case 'tab1':
            $this->displayAccessKeys();
            break;
        case 'tab2':
            $this->displayAccessRules();
            break;
        case 'tab3':
            $this->displayAccessLabels();
            break;
        default:
            // Never happens.
        }

        echo '</div>';
    }

    public function toAccessKeyEdit() {
        if (!current_user_can('manage_options')) {
            wp_die(__('You are not allowed to access this page.', 'wooplus'));
        }
        check_admin_referer('wooplusAccessKeyNonce');
        $keyName = $_POST['key_name'];
        wp_redirect(admin_url('options-general.php?page=wooplusAccessManagement&tab=tab1&edit=' . $keyName));
        exit;
    }

    public function toAccessKeyTranslate() {
        if (!current_user_can('manage_options')) {
            wp_die(__('You are not allowed to access this page.', 'wooplus'));
        }
        check_admin_referer('wooplusAccessKeyNonce');

        $keyCode = $_POST['key_code'];
        wp_redirect(admin_url('options-general.php?page=wooplusAccessManagement&tab=tab1&translate=' . $keyCode));
        exit;
    }

    public function addAccessKey() {
        if (!current_user_can('manage_options')) {
            wp_die(__('You are not allowed to access this page.', 'wooplus'));
        }
        check_admin_referer('wooplusAccessKeyNonce');

        $keyName = $_POST['key_name'];
        if(WooplusDefinedKeys::have($keyName)) {
            wp_redirect(admin_url('options-general.php?page=wooplusAccessManagement&tab=tab1&edit=' . $keyName));
            exit;
            }
        else {
            $onlySite = (isset($_POST['only_site'])) ? get_current_blog_id() : NULL;
            WooplusDefinedKeys::add($keyName, $onlySite);

            wp_redirect(admin_url('options-general.php?page=wooplusAccessManagement&tab=tab1&key_a'));
            exit;
        }
    }

    public function editAccessKey() {
        if (!current_user_can('manage_options')) {
            wp_die(__('You are not allowed to access this page.', 'wooplus'));
        }
        check_admin_referer('wooplusAccessKeyNonce');

        $keyCode = $_POST['key_code'];
        $keyName = $_POST['key_name'];
        $onlySite = (isset($_POST['only_site'])) ? get_current_blog_id() : NULL;
        WooplusDefinedKeys::set($keyCode, $keyName, $onlySite);

        wp_redirect(admin_url('options-general.php?page=wooplusAccessManagement&tab=tab1&key_s'));
        exit;
    }

    public function translateAccessKey() {
        if (!current_user_can('manage_options')) {
            wp_die(__('You are not allowed to access this page.', 'wooplus'));
        }
        check_admin_referer('wooplusAccessKeyNonce');
        
        $keyCode     = $_POST['key_code'];
        $keyName     = $_POST['key_name'];
        $keyLanguage = $_POST['key_language'];
        WooplusTranslatedKeys::set($keyCode, $keyName, $keyLanguage);

        wp_redirect(admin_url('options-general.php?page=wooplusAccessManagement&tab=tab1&key_t'));
        exit;
    }

    public function removeAccessKey() {
        if (!current_user_can('manage_options')) {
            wp_die(__('You are not allowed to access this page.', 'wooplus'));
        }
        check_admin_referer('wooplusAccessKeyNonce');

        $keyCode = $_POST['key_code'];
        WooplusDefinedKeys::remove($keyCode);
        WooplusTranslatedKeys::remove($keyCode);

        wp_redirect(admin_url('options-general.php?page=wooplusAccessManagement&tab=tab1&key_r'));
        exit;
    }

    public function addAccessRule() {
        if (!current_user_can('manage_options')) {
            wp_die(__('You are not allowed to access this page.', 'wooplus'));
        }
        check_admin_referer('wooplusAccessRuleNonce');

        $accessruleCode    = wp_generate_uuid4();
        $accessLabel       = $_POST['label'];
        $accessRequirement = $_POST['requirement'];
        $accessRedirect    = $_POST['redirect'];
        if(empty(WooplusAccessRules::have($accessLabel, $accessRequirement))) {
            // This rule is not already existing:
            WooplusAccessRules::add($accessruleCode, $accessLabel, $accessRequirement, $accessRedirect);
            
            wp_redirect(admin_url('options-general.php?page=wooplusAccessManagement&tab=tab2&rule_a'));
            exit;
        }
        wp_redirect(admin_url('options-general.php?page=wooplusAccessManagement&tab=tab2'));
        exit;
    }

    public function removeAccessRule() {
        if (!current_user_can('manage_options')) {
            wp_die(__('You are not allowed to access this page.', 'wooplus'));
        }
        check_admin_referer('wooplusAccessRuleNonce');

        if(isset($_POST['code'])) {
            $code = $_POST['code'];
            WooplusAccessRules::remove($code);
            
            wp_redirect(admin_url('options-general.php?page=wooplusAccessManagement&tab=tab2&rule_r'));
            exit;
        }
        wp_redirect(admin_url('options-general.php?page=wooplusAccessManagement&tab=tab2'));
        exit;
    }

    public function addAccessLabel() {
        if (!current_user_can('manage_options')) {
            wp_die(__('You are not allowed to access this page.', 'wooplus'));
        }
        check_admin_referer('wooplusAccessLabelNonce');

        $termName = $_POST['new_label_name'];
        $termExists = term_exists($termName, 'wooplus_label');
        if ((!$termExists) && (!empty($termName))) {
            wp_insert_term($termName, 'wooplus_label');
            
            wp_redirect(admin_url('options-general.php?page=wooplusAccessManagement&tab=tab3&label_a'));
            exit;
        }
        wp_redirect(admin_url('options-general.php?page=wooplusAccessManagement&tab=tab3'));
        exit;
    }

    public function removeAccessLabel() {
        if (!current_user_can('manage_options')) {
            wp_die(__('You are not allowed to access this page.', 'wooplus'));
        }
        check_admin_referer('wooplusAccessLabelNonce');

        if(isset($_POST['label_name'])) {
            $termName = $_POST['label_name'];
            $termExists = term_exists($termName, 'wooplus_label');
            if ($termExists) {
                $term = get_term_by('name', $termName, 'wooplus_label');
                $termId = $term->term_id;
                wp_delete_term($termId, 'wooplus_label');

                wp_redirect(admin_url('options-general.php?page=wooplusAccessManagement&tab=tab3&label_r'));
                exit;
            }
        }
        wp_redirect(admin_url('options-general.php?page=wooplusAccessManagement&tab=3'));
        exit;
    }

    private function displayAccessKeys() {
        $currentLanguageCode = get_locale();
        $displayTranslation = ($currentLanguageCode == 'en_US') ? false : true;
        $editTranslation = (isset($_GET['translation'])) ? true : false;

        echo '<div class="wrap">';

        echo '<table>';
        echo '<tr><td>';
        if(isset($_GET['key_a'])) {
            echo '<div id="message" class="updated fade">';
            echo '<p><strong>' . __('New access key was added.', 'wooplus') . '</strong></p>';
            echo '</div>';
        }
        if(isset($_GET['key_s'])) {
            echo '<div id="message" class="updated fade">';
            echo '<p><strong>' . __('The access key was edited.', 'wooplus') . '</strong></p>';
            echo '</div>';
        }
        if(isset($_GET['key_r'])) {
            echo '<div id="message" class="updated fade">';
            echo '<p><strong>' . __('An access key was removed.', 'wooplus') . '</strong></p>';
            echo '</div>';
        }
        if(isset($_GET['key_t'])) {
            echo '<div id="message" class="updated fade">';
            echo '<p><strong>' . __('The access key was translated.', 'wooplus') . '</strong></p>';
            echo '</div>';
        }
        echo '</td></tr>';
        echo '</table>';


        echo '<table id="t01">';
        echo '<tr>';
        echo '<th class="col-keyname">' . __('Access Key Name', 'wooplus') . '</th>';
        if($displayTranslation) {
            echo '<th class="col-translation">' . __('Translation', 'wooplus') . '</th>';
        }
        echo '<th class="col-siteonly">' . __('This Site Only', 'wooplus') . '</th>';
        echo '<th class="col-cmd" colspan="3">' . __('Cmd', 'wrowooplus') . '</th>';
        echo '</tr>';
        echo '<tr>';
        echo '<form method="post" action="admin-post.php">';
        if(isset($_GET['translate'])) {
            $accessKeyCode = $_GET['translate'];
            $definedKey = WooplusDefinedKeys::get($accessKeyCode);
            $translatedKey = WooplusTranslatedKeys::get($accessKeyCode, $currentLanguageCode);
            $onlySite = (isset($definedKey->only_site)) ? ' checked' : '';
            echo '<td>' . $definedKey->key_name . '</td>';
            echo '<td><input type="text" name="key_name" id="key_name" value="' . $translatedKey->key_name . '" maxlength="255"></td>';
            echo '<td><input type="checkbox" name="only_site"' . $onlySite . ' disabled></td>';
            echo '<td colspan="3"><input type="submit" value="' . __('OK', 'wooplus') . '." class="button-primary"/></td>';
            echo '<input type="hidden" name="action" value="translateWooplusAccessKey"/>';
            echo '<input type="hidden" id="key_code" name="key_code" value="' . $definedKey->key_code . '">';
            echo '<input type="hidden" id="key_language" name="key_language" value="' . $currentLanguageCode . '">';
            }
        else if(isset($_GET['edit'])) {
            $keyCode = $_GET['edit'];
            $definedKey = WooplusDefinedKeys::get($keyCode);
            $keyName  = (isset($definedKey->key_name)) ? $definedKey->key_name : '';
            $onlySite = (isset($definedKey->only_site)) ? ' checked' : '';
            echo '<td><input type="text" name="key_name" id="key_name" value="' . $keyName . '" maxlength="255"></td>';
            if($displayTranslation) {
                echo '<td></td>';
            }
            echo '<td><input type="checkbox" name="only_site"' . $onlySite . '></td>';
            echo '<td colspan="3"><input type="submit" value="' . __('OK', 'wooplus') . '." class="button-primary"/></td>';
            echo '<input type="hidden" name="action" value="editWooplusAccessKey"/>';
            echo '<input type="hidden" id="key_code" name="key_code" value="' . $definedKey->key_code . '">';
        }
        else {
            echo('<td><input type="text" name="key_name" id="key_name" maxlength="255"></td>');
            if($displayTranslation) {
                echo '<td></td>';
            }
            echo '<td><input type="checkbox" name="only_site"></td>';
            echo('<td colspan="3"><input type="submit" value="' . __('Add', 'wooplus') . '." class="button-primary"/></td>');
            echo '<input type="hidden" name="action" value="addWooplusAccessKey"/>';
        }
        wp_nonce_field('wooplusAccessKeyNonce');
        echo '</form>';
        echo '</tr>';

        $currentSite = get_current_blog_id();
        $rows = WooplusDefinedKeys::list();
        foreach($rows as $row) {
            if((empty($row->only_site)) || ($row->only_site == $currentSite)) {
                $omitCode = NULL;
                if(isset($_GET['edit'])) {
                    $omitCode = $_GET['edit'];
                }
                else if(isset($_GET['translate'])) {
                    $omitCode = $_GET['translate'];
                }
                if(!isset($omitCode) || ($omitCode <> $row->key_code)) {
                    $onlySite = (isset($row->only_site)) ? 'o' : '';
                    echo '<tr>';
                    
                    echo '<td>' . $row->key_name . '</td>';
                    
                    if($displayTranslation) {
                        $translatedKey = WooplusTranslatedKeys::get($row->key_code, $currentLanguageCode);
                        $translatedKeyName = (empty($translatedKey->key_name)) ? '' : $translatedKey->key_name;
                        echo '<td>' . $translatedKeyName . '</td>';
                    }
        
                    echo '<td>' . $onlySite . '</td>';
                    
                    echo '<td>';
                    echo '<form method="post" action="admin-post.php">';
                    echo '<input type="submit" value="' . __('Edit', 'wooplus') . '." class="button-primary"/>';
                    echo '<input type="hidden" name="action" value="toWooplusAccessKeyEdit"/>';
                    echo '<input type="hidden" id="key_name" name="key_name" value="' . $row->key_code . '">';
                    wp_nonce_field('wooplusAccessKeyNonce');
                    echo '</form>';
                    echo '</td>';
        
                    echo '<td>';
                    echo '<form method="post" action="admin-post.php">';
                    echo '<input type="submit" value="' . __('Translate', 'wooplus') . '." class="button-primary"/>';
                    echo '<input type="hidden" name="action" value="toWooplusAccessKeyTranslate"/>';
                    echo '<input type="hidden" id="key_code" name="key_code" value="' . $row->key_code . '">';
                    wp_nonce_field('wooplusAccessKeyNonce');
                    echo '</form>';
                    echo '</td>';
        
                    echo '<td>';
                    echo '<form method="post" action="admin-post.php">';
                    echo '<input type="submit" value="' . __('Remove', 'wooplus') . '." class="button-primary"/>';
                    echo '<input type="hidden" name="action" value="removeWooplusAccessKey"/>';
                    echo '<input type="hidden" id="key_code" name="key_code" value="' . $row->key_code . '">';
                    wp_nonce_field('wooplusAccessKeyNonce');
                    echo '</form>';
                    echo '</td>';
        
                    echo '</tr>';
                }                
            }
        }
        echo '</table>';
        echo '</div>';
    }

    private function displayAccessRules() {
        $currentSite = get_current_blog_id();
        $currentLanguageCode = get_locale();
        $displayTranslation = ($currentLanguageCode == 'en_US') ? false : true;

        echo '<div class="wrap">';

        echo '<table>';
        echo '<tr><td>';
        if(isset($_GET['rule_a'])) {
            echo '<div id="message" class="updated fade">';
            echo '<p><strong>' . __('New page access rule was added.', 'wooplus') . '</strong></p>';
            echo '</div>';
        }
        if(isset($_GET['rule_r'])) {
            echo '<div id="message" class="updated fade">';
            echo '<p><strong>' . __('A page access rule was removed.', 'wooplus') . '</strong></p>';
            echo '</div>';
        }
        echo '</td></tr>';
        echo '</table>';

        echo '<table id="t01">';
        echo '<tr>';
        echo '<th class="col-label">' . __('Label', 'wooplus') . '</th>';
        echo '<th class="col-requirement">' . __('Requirement', 'wooplus') . '</th>';
        echo '<th class="col-redirect">' . __('Redirect', 'wooplus') . '</th>';
        echo '<th class="col-cmd">' . __('Cmd', 'wooplus') . '</th>';
        echo '</tr>';
        echo '<form method="post" action="admin-post.php">';
        echo '<td>';
        echo '<input type="hidden" name="action" value="addWooplusAccessRule" />';
        wp_nonce_field('wooplusAccessRuleNonce');
        echo '<select name ="label">';
        $accessLabelsQuery = new WP_Term_Query(array(
            'taxonomy'   => 'wooplus_label',
            'orderby'    => 'name',
            'order'      => 'ASC',
            'hide_empty' => false,
        ));
        $accessLabels = $accessLabelsQuery->get_terms();
        foreach($accessLabels as $accessLabel) {
            $accessLabelName = $accessLabel->name;
            echo '<option value ="' . $accessLabelName . '">' . $accessLabelName . '</option>';
        }
        echo '</select>';
        echo '</td><td>';
        echo '<select name="requirement">';
        echo '<option value="logged-in">[' . __('Logged-in', 'wooplus') . ']</option>';
        $definedKeys = WooplusDefinedKeys::list();
        foreach($definedKeys as $definedKey) {
            if((empty($definedKey->only_site)) || ($definedKey->only_site == $currentSite)) {
                $keyName = $definedKey->key_name;
                if($displayTranslation) {
                    $translatedKey = WooplusTranslatedKeys::get($definedKey->key_code, $currentLanguageCode);
                    $keyName = (empty($translatedKey->key_name)) ? $keyName : $translatedKey->key_name;
                }
                echo '<option value="' . $definedKey->key_code . '">' . $keyName . '</option>';
            }
        }
        echo '</select>';
        echo '</td><td>';
        echo '<select name="redirect">';
        echo '<option value="404">' . __('Page: 404', 'wooplus') . '</option>';
        echo '<option value="home">' . __('Page: Home', 'wooplus') . '</option>';
        echo '<option value="login">' . __('Page: Login', 'wooplus') . '</option>';
        echo '</select>';
        echo '</td><td>';
        echo '<input type="submit" value="+" class="button-primary"/>';
        echo '</td>';
        echo '</form>';
        $accessRules = WooplusAccessRules::list();
        foreach($accessRules as $accessRule) {
            echo '<tr>';
            echo '<form method="post" action="admin-post.php">';
            echo '<td>';
            echo '<input type="hidden" name="action" value="removeWooplusAccessRule' . '"/>';
            echo '<input type="hidden" id="code" name="code" value="' . $accessRule->code . '">';
            wp_nonce_field('wooplusAccessRuleNonce');
            echo $accessRule->label;
            echo '</td>';
            $requirement = $accessRule->requirement;
            if($requirement == 'logged-in') {
                echo '<td>' . __('Logged-in', 'wooplus') . '</td>';
            }
            else {
                $definedKey = WooplusDefinedKeys::get($accessRule->requirement);
                $translatedKey = WooplusTranslatedKeys::get($accessRule->requirement, $currentLanguageCode);
                $keyName = (empty($translatedKey->key_name)) ? $definedKey->key_name : $translatedKey->key_name;
                echo '<td>' . $keyName . '</td>';
            }
            echo '<td>' . $accessRule->redirect . '</td>';
            echo '<td><input type="submit" value="x" class="button-primary"/></td>';
            echo '</form>';
            echo '</tr>';
        }
        echo '</table>';
        echo '</div>';
    }

    private function displayAccessLabels() {
        echo '<div class="wrap">';

        echo '<table>';
        echo '<tr><td>';
        if(isset($_GET['label_a'])) {
            echo '<div id="message" class="updated fade">';
            echo '<p><strong>' . __('New access label was added.', 'wooplus') . '</strong></p>';
            echo '</div>';
        }
        if(isset($_GET['label_r'])) {
            echo '<div id="message" class="updated fade">';
            echo '<p><strong>' . __('An access label was removed.', 'wooplus') . '</strong></p>';
            echo '</div>';
        }
        echo '</td></tr>';
        echo '</table>';

        echo '<table id="t01">';
        echo '<tr>';
        echo '<th class="col-labelname">' . __('Access Label Name', 'wooplus') . '</th>';
        echo '<th class="col-cmd">' . __('Cmd', 'wooplus') . '</th>';
        echo '</tr>';
        echo '<tr>';
        echo '<tr>';
        echo '<form method="post" action="admin-post.php">';
        echo '<td>';
        echo '<input type="hidden" name="action" value="addWooplusAccessRule" />';
        echo '<input type="text" name="new_label_name" id="new_label_name" maxlength="255">';
        echo '<input type="hidden" name="action" value="addWooplusAccessLabel" />';
        wp_nonce_field('wooplusAccessLabelNonce');
        echo '</td>';
        echo '<td><input type="submit" value="+" class="button-primary"/></td>';
        echo '</form>';
        echo '</tr>';
        $accessLabelsQuery = new WP_Term_Query(array(
            'taxonomy'   => 'wooplus_label',
            'orderby'    => 'name',
            'order'      => 'ASC',
            'hide_empty' => false,
        ));
        $accessLabels = $accessLabelsQuery->get_terms();
        foreach($accessLabels as $accessLabel) {
            $accessLabelName = $accessLabel->name;
            echo '<tr>';
            echo '<form method="post" action="admin-post.php">';
            echo '<td>';
            echo '<input type="hidden" name="action" value="removeWooplusAccessLabel' . '"/>';
            echo '<input type="hidden" id="label_name" name="label_name" value="' . $accessLabelName . '">';
            wp_nonce_field('wooplusAccessLabelNonce');
            echo $accessLabelName;
            echo '</td>';
            echo '<td><input type="submit" value="x" class="button-primary"/></td>';
            echo '</form>';
            echo '</tr>';
        }
        echo '</table>';
        echo '</div>';
    }
}
?>