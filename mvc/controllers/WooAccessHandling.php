<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

require_once(plugin_dir_path(__FILE__) . '../models/WooplusProducts.php');
require_once(plugin_dir_path(__FILE__) . '../models/WooplusAccessKeys.php');
require_once(plugin_dir_path(__FILE__) . '../models/WooplusAccessRules.php');
require_once(plugin_dir_path(__FILE__) . '../models/WooplusSubscriptions.php');

class WooplusAccessHandling {

    public static function watchSubscriptions() {
        WooplusSubscriptions::removeExpired();
        // ...First remove all expired subscriptions; i.e. mark them invalid.
        WooplusAccessKeys::removeExpired();
        // ...Then remove all so rexpired access-keys; i.e. mark them invalid basedu upon the invalidated subscriptions.
    }

    public function __construct() {
        add_action('woocommerce_order_status_completed', array($this, 'handlePaymentProcessed'));
        add_action('woocommerce_order_refunded', array($this, 'handlePaymentRefunded'), 10, 2 );
        add_action('woocommerce_product_query', array($this, 'queryAccessibleProducts'));
        add_filter('woocommerce_related_products', array($this, 'filterRelatedProducts'), 10, 3);
        add_action('template_redirect', array($this, 'grantPageAccess'));
    }

    public function handlePaymentProcessed($orderId) {
        $order = wc_get_order($orderId);
        $userId = $order->get_user_id();
        $items = $order->get_items(); 
        foreach($items as $item) {
            $productId = $item->get_product_id();
            $itemId = $item->get_id();
            $subscriptionCode = NULL;

            $subscriptionPeriodLength = get_post_meta($productId, 'subscription_period_length', true);
            if(!empty($subscriptionPeriodLength)) {
                // The order item is a subscription:
                $subscriptionUserId = get_post_meta($orderId, '_customer_user', true);
                $subscriptionPeriodUnit = get_post_meta($productId, 'subscription_period_unit', true);
                $beginDate = gmdate('Y-m-d');

                $n = $item->get_quantity();
                for($i=0; $i<$n; $i++) {
                    WooplusSubscriptions::add
                    ( $productId
                    , $subscriptionUserId
                    , $subscriptionPeriodLength
                    , $subscriptionPeriodUnit
                    , $beginDate
                    );
                    // ...When an active subscription is present, then this is implicitly renewed and the periods counter increased by one.
                    // ...When this order reaches the complete status multiple times, then the quantity amount of subscriptions is applied each time!
                    // ...The instance of reaching the complete status multiple times must be handled manually within the database directly!
                }
                $subscription = WooplusSubscriptions::get($productId, $subscriptionUserId);
                $subscriptionCode = $subscription->code;
                wc_update_order_item_meta($itemId, 'subscription_code', $subscriptionCode);
            }

            $gainingKeysString = get_post_meta($productId, 'access_gaining_keys', true);
            if(!empty($gainingKeysString)) {
                // The order item gains the user access-keys:
                $gainingKeyCodes = (empty($gainingKeysString)) ?  array() : explode(',', $gainingKeysString);
                foreach($gainingKeyCodes as $gainingKeyCode) {
                    $orderItemDetails = array();
                    $orderItemDetails['order_id']          = $orderId;
                    $orderItemDetails['order_item_id']     = $itemId;
                    $orderItemDetails['subscription_code'] = $subscriptionCode;
                    WooplusAccessKeys::add($gainingKeyCode, $userId, $orderItemDetails);

                    // ...Generate the access-key with a valid ('o') stage with also providing the order item details.
                    // ...When this order reaches the complete status muliple times, then no additional access key is created.
                }
                wc_update_order_item_meta($itemId, 'access_gaining_keys', $gainingKeysString);
            }
        }
    }

    public function handlePaymentRefunded($originalOrderId, $refundedOrderId) {
        $originalOrder = wc_get_order($originalOrderId);
        $refundedOrder = wc_get_order($refundedOrderId);
        $userId        = $originalOrder->get_user_id();
        $originalItems = $originalOrder->get_items();
        looping: foreach($originalItems as $itemId => $originalItem) {
            $productId = $originalItem->get_product_id();
            $originalQuantity = $originalItem->get_quantity();
            $refundedItem = $refundedOrder->get_item($itemId);
            if($refundedItem) {
                $refundedQuantity = $refundedItem->get_quantity();
                $remainingQuantity = $originalQuantity - $refundedQuantity;

                $subscriptionPeriodLength = get_post_meta($productId, 'subscription_period_length', true);
                if(!empty($subscriptionPeriodLength)) {
                    // The order item is a subscription:
                    for($i=0; $i<$refundedQuantity; $i++) {
                        WooplusSubscriptions::remove($productId, $userId);
                    }
                }
 
                $haveNot = !WooplusSubscriptions::have($productId, $userId);
                if(($remainingQuantity < 1) && !WooplusSubscriptions::have($productId, $userId)) {
                    // We have a fully refunded item, which has no active subscriptions:
                    $gainingKeysString = wc_get_order_item_meta($itemId, 'access_gaining_keys', true);
                    $gainingKeyCodes = (empty($gainingKeysString)) ?  array() : explode(',', $gainingKeysString);
                    foreach($gainingKeyCodes as $gainingKeyCode) {
                        $orderItemDetails = array();
                        $orderItemDetails['order_id'] = $originalOrderId;
                        $orderItemDetails['order_item_id'] = $itemId;
                        WooplusAccessKeys::remove($gainingKeyCode, $userId, $orderItemDetails);
                    }
                }
            }
        }
    }

    public function queryAccessibleProducts($query) {
        $accessibleProductIds = WooplusProducts::listAccessible();
        if(empty($accessibleProductIds)) {
            $accessibleProductIds = array(-1);
            // ...The array containing an illegal product ID is needed, since an empty array is interpreted as no filter.
        }
        $query->set('post__in', $accessibleProductIds);
    }

    public function filterRelatedProducts($relatedProducts, $productId, $args) {
        $filteredRelatedProducts = array();
        foreach($relatedProducts as $relatedProduct) {
            if (is_object($relatedProduct) && isset($relatedProduct->ID) && WooplusProducts::isAccessible($relatedProduct->ID)) {
                $filteredRelatedProducts[] = $relatedProduct;
            }
        }

        return empty($filteredRelatedProducts) ? array(-1) : $filteredRelatedProducts;
        // ...The array containing an illegal product ID is needed, since an empty array is interpreted as no filter.
    }

    public function grantPageAccess() {
        if (class_exists('BuddyPress')) {
            if(!is_user_logged_in() && (bp_is_directory() || bp_is_user())) {
                $this->redirect404();
                // ...For BuddyPress illegal access always redirect hard coded to the 404 page.
                die;
            }
        }

        if(is_product() && !WooplusProducts::isAccessible(get_the_ID())) {
            $this->redirect404();
            // ...For illegally accessing a not allowed product page always redirect hard coded to the 404 page.
            die;
        }

        $currentPageId = get_the_ID();
        $accessLabelId = get_post_meta($currentPageId, '_access_label', true);
        // ...The last parameter (i.e. single) is set to true indicating only to return a single value and not all.
        $accessLabel = get_term($accessLabelId, 'wooplus_label');
        if(!is_wp_error($accessLabel)) {
            $accessLabelName = $accessLabel->name;
            $accessRules = WooplusAccessRules::listByLabel($accessLabelName);
            // ...Lists the requirements in ascending order for this page.
            foreach($accessRules as $accessRule) {
                $this->checkAccess($accessRule);
            }
        }
    }

    private function checkAccess($accessRule) {
        $requirement = $accessRule->requirement;
        if(($requirement == 'logged-in') && (! is_user_logged_in())) {
            // This is the requirement to be logged in:
            $this->performRedirect($accessRule);
        }
        else {
            // This is the requirement to own an active key:
            $definedKeyCode = $requirement;
            $userId = get_current_user_id();
            if(!WooplusAccessKeys::have($definedKeyCode, $userId)) {
                $this->performRedirect($accessRule);
            }
        }
    }

    private function performRedirect($accessRule) {
        switch($accessRule->redirect) {
        case '404':
            $this->redirect404();
            die();
            break;
        case 'home':
            wp_redirect(get_home_url());
            die;
            break;
        case 'login':
            global $wp;
            wp_redirect(wp_login_url($wp->request));
            // ...The URL-query will be stripped, since the login-redirect works itself with a query.
            die;
            break;
        default:
            // Never happens.
        }
    }

    private function redirect404() {
        global $wp_query;
        
        $wp_query->set_404();
        status_header(404);
        get_template_part(404); 
    }
}
?>