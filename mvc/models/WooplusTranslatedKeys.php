<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

class WooplusTranslatedKeys {

    public static function exists() {
        global $wpdb;
        
        require_once(ABSPATH.'wp-admin/includes/upgrade.php');
        
        $charsetCollate = $wpdb->get_charset_collate();
        // ...Be sure to obtain the configured charset!
        $tableName = $wpdb->base_prefix.'wooplus_translatedkeys';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $sqlString
        = "CREATE TABLE ". $tableName . " ("
        . "code VARCHAR(36) NOT NULL, "
        // ...This is an UUIDv4 having the format "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".
        . "key_code VARCHAR(36) NOT NULL, "
        // ...This is an UUIDv4 having the format "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".
        . "key_name VARCHAR(255) NOT NULL, "
        // ...The TRANSLATED name of the defined key.
        . "key_language VARCHAR(5) NOT NULL, "
        // ...The language code of the key name translation.
        . "PRIMARY KEY  (key_code), "
        // ...Be sure to have TWO(!) spaces after "PRIMARY KEY"!
        . "KEY index_keycode (key_code), "
        . "KEY index_keylanguage (key_language)"
        // ...The idea behind the indices is to narraw DB-internally the data quickly to a small set of data.
        . ") $charsetCollate;"
        ;
        dbDelta($sqlString);
        // ...Modify the WP-database based on the SQL statement.
    }

    public static function get($keyCode, $keyLanguage) {
        global $wpdb;

        $tableName = $wpdb->base_prefix.'wooplus_translatedkeys';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $sqlString
        = "SELECT code, key_code, key_name, key_language"
        . " FROM " . $tableName
        . " WHERE key_code = '" . $keyCode . "'"
        . " AND key_language = '" . $keyLanguage . "'"
        ;

        return $wpdb->get_row($sqlString);
    }

    public static function getByName($keyName, $keyLanguage) {
        global $wpdb;

        $tableName = $wpdb->base_prefix.'wooplus_translatedkeys';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $sqlString
        = "SELECT code, key_code, key_name, key_language"
        . " FROM " . $tableName
        . " WHERE key_name = '" . $keyName . "'"
        . " AND key_language = '" . $keyLanguage . "'"
        ;

        return $wpdb->get_row($sqlString);
    }

    public static function set($keyCode, $keyName, $keyLanguage) {
        global $wpdb;

        $tableName = $wpdb->base_prefix.'wooplus_translatedkeys';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $row = WooplusTranslatedKeys::get($keyCode, $keyLanguage);
        if(isset($row->code)) {
            // Update the existing row:
            $code = $row->code;
            $updateData = array
            ( 'key_code'     => $keyCode
            , 'key_name'     => $keyName
            , 'key_language' => $keyLanguage
            );
            $updateWhere = array
            ( 'code'         => $code
            );
            $updateFormat = array('%s', '%s', '%s');
            $wpdb->update($tableName, $updateData, $updateWhere, $updateFormat);
            // ...Update the data in the entity table.
        }
        else {
            // Insert a new row:
            $code = wp_generate_uuid4();
            $insertData = array
            ( 'code'         => $code
            , 'key_code'     => $keyCode
            , 'key_name'     => $keyName
            , 'key_language' => $keyLanguage
            );
    
            $insertFormat = array('%s', '%s', '%s', '%s');
            $wpdb->insert($tableName, $insertData, $insertFormat);
            // ...Inserts the data into the entity table.(
        }
    }

    public static function remove($keyCode) {
        global $wpdb;

        $tableName = $wpdb->base_prefix.'wooplus_translatedkeys';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $deleteData = array('key_code' => $keyCode);
        $wpdb->delete($tableName, $deleteData);
        // ...Delete the data from the entity table.
    }
}
?>