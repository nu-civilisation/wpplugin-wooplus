<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

class WooplusAccessKeys {

    public static function exists() {
        global $wpdb;
        
        require_once(ABSPATH.'wp-admin/includes/upgrade.php');
        $charsetCollate = $wpdb->get_charset_collate();
        // ...Be sure to obtain the configured charset!
        
        $tableName = $wpdb->base_prefix . 'wooplus_accesskeys';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $sqlString
        = "CREATE TABLE " . $tableName . " ("
        . "code VARCHAR(36) NOT NULL, "
        // ...This is an UUIDv4 having the format "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".
        . "user_id INT(20) NOT NULL, "
        // ...This denotes the WP user_id of the owner of this entity.
        . "key_code VARCHAR(36) NOT NULL, "
        // ...This is an UUIDv4 having the format "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".
        . "tackled VARCHAR(20) NOT NULL, "
        // ...This is a date in UTC string format without any time zones comprising of 20 characters: "YYYY-MM-DDThh:mm:ssZ".
        . "stage VARCHAR(1) NOT NULL, "
        // ...The stages can be: unused (' '), validated ('o') and invalidated ('x').
        . "PRIMARY KEY  (code), "
        // ...Be sure to have TWO(!) spaces after "PRIMARY KEY"!
        . "KEY index_userid (user_id), "
        . "KEY index_keycode (key_code), "
        . "KEY index_stage (stage)"
        // ...The idea behind the indices is to narraw DB-internally the data quickly to a small set of data.
        . ") $charsetCollate;"
        ;
        dbDelta($sqlString);
        // ...Modify the WP-database based on the SQL statement.

        $tableName = $wpdb->base_prefix . 'wooplus_accesskeys2orderitems';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $sqlString
        = "CREATE TABLE " . $tableName . " ("
        . "code VARCHAR(22) NOT NULL, "
        // ...This is a date in UTC string format without any time zones comprising of 20 characters plus two digits from the random number: "YYYY-MM-DDThh:mm:ssZ99".
        . "access_key_code VARCHAR(36) NOT NULL, "
        // ...This is an UUIDv4 having the format "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".
        . "blog_site_id INT(10) NOT NULL, "
        // ...The ID of the blog site within the multisite scenario.
        . "order_id INT(20) NOT NULL, "
        // ...The order ID of the order item.
        . "order_item_id INT(20) NOT NULL, "
        // ...The order item ID.
        . "subscription_code VARCHAR(36), "
        // ...This is an UUIDv4 having the format "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".
        . "PRIMARY KEY  (code), "
        // ...Be sure to have TWO(!) spaces after "PRIMARY KEY"!
        . "KEY index_accesskeycode (access_key_code), "
        . "KEY index_blogsiteid (blog_site_id), "
        . "KEY index_orderid (order_id)"
        // ...The idea behind the indices is to narraw DB-internally the data quickly to a small set of data.
        . ") $charsetCollate;"
        ;
        dbDelta($sqlString);
        // ...Modify the WP-database based on the SQL statement.
    }

    public static function have($keyCode, $userId) {
        global $wpdb;

        if($userId == 0) {
            return false;
            // ...The guest user (i.e. the not logged in user) does per definition not have any access keys.
        }

        $tableName = $wpdb->base_prefix . 'wooplus_accesskeys';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $sqlString
        = "SELECT code "
        . " FROM " . $tableName
        . " WHERE user_id = " . $userId . ""
        . " AND key_code = '" . $keyCode . "'"
        . " AND stage = 'o'"
        ;
        $result = $wpdb->get_var($sqlString);
        // ...The result is a single scalar value.

        return empty($result) ? false : true;
    }

    public static function get($keyCode, $userId) {
        global $wpdb;

        $accessKeysTableName = $wpdb->base_prefix . 'wooplus_accesskeys';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $definedKeysTableName = $wpdb->base_prefix . 'wooplus_definedkeys';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $sqlString
        = "SELECT"
        . " ak.code AS code,"
        . " ak.user_id AS user_id,"
        . " dk.key_code AS key_code,"
        . " dk.key_name AS key_name,"
        . " dk.only_site AS only_site,"
        . " ak.tackled AS tackled,"
        . " ak.stage AS stage"
        . " FROM " . $accessKeysTableName . " ak"
        . " INNER JOIN " . $definedKeysTableName . " dk"
        . " ON ak.key_code = dk.key_code"
        . " WHERE ak.key_code = '" . $keyCode . "'"
        . " AND ak.user_id = " . $userId . ""
        . " AND ak.stage = 'o'"
        ;

        return $wpdb->get_row($sqlString);
        // ...Only one entity is expected.
    }

    public static function getOrderDetails($keyCode, $userId, $orderItemId) {
        global $wpdb;

        $accessKey = WooplusAccessKeys::get($keyCode, $userId);
        $tableName = $wpdb->base_prefix . 'wooplus_accesskeys2orderitems';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!

        $sqlString
        = "SELECT code, access_key_code, blog_site_id, order_id, order_item_id, subscription_code"
        . " FROM " . $tableName
        . " WHERE access_key_code = '" . $accessKey->code . "'"
        . " AND blog_site_id = " . get_current_blog_id()
        . " AND order_item_id = " . $orderItemId . ""
        ;

        return $wpdb->get_row($sqlString);
        // ...Only one entity is expected.
    }

    public static function add($keyCode, $userId, $orderItemDetails) {
        global $wpdb;

        $accessKey = WooplusAccessKeys::get($keyCode, $userId);
        $adding = empty($accessKey->code);
        $accessKeyCode = ($adding) ? wp_generate_uuid4() : $accessKey->code;
        if($adding) {
            $tackled = gmdate('Y-m-d\TH:i:s\Z');
            // ...Formats the current timestamp in the format "YYYY-MM-DDThh:mm:ssZ"; these are 20 characters.
            $tableName = $wpdb->base_prefix . 'wooplus_accesskeys';
            // ...Be sure to obtain the database prefix to be consistent in naming!
            // ...The entity table is a WordPress blog site GENERAL table!
            $insertData = array
            ( 'code'     => $accessKeyCode
            , 'user_id'  => $userId
            , 'key_code' => $keyCode
            , 'tackled'  => $tackled
            , 'stage'    => 'o'
            );
            $insertFormat = array('%s', '%d', '%s', '%s', '%s');
            $wpdb->insert($tableName, $insertData, $insertFormat);
            // ...Inserts the data into the entity table.
        }

        if(!empty($orderItemDetails)) {
            $tableName = $wpdb->base_prefix . 'wooplus_accesskeys2orderitems';
            // ...Be sure to obtain the database prefix to be consistent in naming!
            // ...The entity table is a WordPress blog site GENERAL table!
            $insertData = array
            ( 'code'              => gmdate('Y-m-d\TH:i:s\Z') . sprintf("%02d", mt_rand(0, 99))
            , 'access_key_code'   => $accessKeyCode
            , 'blog_site_id'      => get_current_blog_id()
            // ...In a single site scenario this function constantly returns 1.
            , 'order_id'          => $orderItemDetails['order_id']
            , 'order_item_id'     => $orderItemDetails['order_item_id']
            , 'subscription_code' => $orderItemDetails['subscription_code']
            );
            $insertFormat = array('%s', '%s', '%d', '%d', '%d', '%s');
            $wpdb->insert($tableName, $insertData, $insertFormat);
        }

        if($adding) {
            do_action('wooplus_accesskey_added', $accessKeyCode);
            // ...Trigger all hooks listening to this custom WP event.
        }
    }

    public static function remove($keyCode, $userId, $orderItemDetails) {
        global $wpdb;

        $accessKey = WooplusAccessKeys::get($keyCode, $userId);
        $tableName = $wpdb->base_prefix . 'wooplus_accesskeys2orderitems';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        
        if(isset($accessKey->code) && isset($orderItemDetails)) {
            $sqlString
            = "SELECT code "
            . " FROM " . $tableName
            . " WHERE access_key_code = '" . $accessKey->code . "'"
            . " AND blog_site_id = " . get_current_blog_id() . ""
            . " AND order_id = " . $orderItemDetails['order_id'] . ""
            . " AND order_item_id = " . $orderItemDetails['order_item_id'] . ""
            ;
            $code = $wpdb->get_var($sqlString);
            // ...The result is a single scalar value.
        
            $deleteData = array('code' => $code);
            $wpdb->delete($tableName, $deleteData);
            // ...Delete the data from the entity table.
        }
        
        $sqlString
        = "SELECT code "
        . " FROM " . $tableName
        . " WHERE access_key_code = '" . $accessKey->code . "'"
        ;
        $code = $wpdb->get_var($sqlString);
        // ...The result is a single scalar value.
        if(empty($code)) {
            $tackled = gmdate('Y-m-d\TH:i:s\Z');
            // ...Formats the current timestamp in the format "YYYY-MM-DDThh:mm:ssZ"; these are 20 characters.
            $tableName = $wpdb->base_prefix . 'wooplus_accesskeys';
            // ...Be sure to obtain the database prefix to be consistent in naming!
            // ...The entity table is a WordPress blog site GENERAL table!
            $updateData = array
            ( 'tackled' => $tackled
            , 'stage'   => 'x'
            // ...Set the stage to invalidated.
            );
            $updateWhere = array
            ( 'code'    => $accessKey->code
            );
            $updateFormat = array('%s', '%s');
            $wpdb->update($tableName, $updateData, $updateWhere, $updateFormat);
            // ...Update the data in the entity table.

            do_action('wooplus_accesskey_removed', $accessKey->code);
            // ...Trigger all hooks listening to this custom WP event.
        }
    }

    public static function removeExpired() {
        global $wpdb;

        $accessekeysTableName = $wpdb->base_prefix . 'wooplus_accesskeys';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $accesskeys2orderitemsTableName = $wpdb->base_prefix . 'wooplus_accesskeys2orderitems';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $subscriptionsTableName = $wpdb->prefix . 'wooplus_subscriptions';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site SPECIFIC table!
        $sqlString
        = "UPDATE " . $accessekeysTableName . " ak"
        . " INNER JOIN " . $accesskeys2orderitemsTableName . " ao"
        . " ON ak.code = ao.access_key_code"
        . " INNER JOIN " . $subscriptionsTableName . " s"
        . " ON ao.subscription_code = s.code"
        . " SET ak.stage = 'x'"
        . " WHERE ak.stage = 'o'"
        . " AND s.stage = 'x'"
        . " AND ao.blog_site_id = " . get_current_blog_id() . ""
        ;

        $wpdb->query($wpdb->prepare($sqlString));
        // ...Update the data in the entity table.
    }
}
?>