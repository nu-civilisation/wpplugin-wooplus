<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

class WooplusDefinedKeys {

    public static function exists() {
        global $wpdb;
        
        require_once(ABSPATH.'wp-admin/includes/upgrade.php');
        
        $charsetCollate = $wpdb->get_charset_collate();
        // ...Be sure to obtain the configured charset!
        $tableName = $wpdb->base_prefix.'wooplus_definedkeys';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $sqlString
        = "CREATE TABLE ". $tableName . " ("
        . "key_code VARCHAR(36) NOT NULL, "
        // ...This is an UUIDv4 having the format "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".
        . "key_name VARCHAR(255) NOT NULL, "
        // ...The name of the defined key.
        . "only_site INT(10) NULL, "
        // ...This denotes, if the defined key is vaild only in the ID of the specified blog (i.e. WP site of the WP multisite instance).
        . "PRIMARY KEY  (key_code)"
        // ...Be sure to have TWO(!) spaces after "PRIMARY KEY"!
        . ") $charsetCollate;"
        ;
        dbDelta($sqlString);
        // ...Modify the WP-database based on the SQL statement.
    }

    public static function have($keyName) {
        global $wpdb;

        $tableName = $wpdb->base_prefix.'wooplus_definedkeys';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $sqlString
        = "SELECT key_code"
        . " FROM " . $tableName
        . " WHERE key_name = '" . $keyName . "'"
        ;
        $result = $wpdb->get_var($sqlString);
        // ...The result is a single scalar value.

        return empty($result) ? false : true;
    }

    public static function list() {
        global $wpdb;

        $tableName = $wpdb->base_prefix.'wooplus_definedkeys';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $sqlString
        = "SELECT key_code, key_name, only_site"
        . " FROM " . $tableName
        . " ORDER BY key_name ASC"
        ;

        return $wpdb->get_results($sqlString);
    }

    public static function get($keyCode) {
        global $wpdb;

        $tableName = $wpdb->base_prefix.'wooplus_definedkeys';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $sqlString
        = "SELECT key_code, key_name, only_site"
        . " FROM " . $tableName
        . " WHERE key_code = '" . $keyCode . "'"
        ;

        return $wpdb->get_row($sqlString);
    }

    public static function getByName($keyName) {
        global $wpdb;

        $tableName = $wpdb->base_prefix.'wooplus_definedkeys';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $sqlString
        = "SELECT key_code, key_name, only_site"
        . " FROM " . $tableName
        . " WHERE key_name = '" . $keyName . "'"
        ;

        return $wpdb->get_row($sqlString);
    }

    public static function add($keyName, $onlySite) {
        global $wpdb;

        $tableName = $wpdb->base_prefix.'wooplus_definedkeys';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $keyCode = wp_generate_uuid4();
        $insertData = array
        ( 'key_code'  => $keyCode
        , 'key_name'  => $keyName
        , 'only_site' => $onlySite
        );

        $insertFormat = array('%s', '%s', '%d');
        $wpdb->insert($tableName, $insertData, $insertFormat);
        // ...Inserts the data into the entity table.(
    }

    public static function set($keyCode, $keyName, $onlySite) {
        global $wpdb;

        $tableName = $wpdb->base_prefix.'wooplus_definedkeys';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $updateData = array
        ( 'key_name'  => $keyName
        , 'only_site' => $onlySite
        );
        $updateWhere = array
        ( 'key_code'  => $keyCode
        );
        $updateFormat = array('%s', '%d');
        $wpdb->update($tableName, $updateData, $updateWhere, $updateFormat);
        // ...Update the data in the entity table.
    }

    public static function remove($keyCode) {
        global $wpdb;

        $tableName = $wpdb->base_prefix.'wooplus_definedkeys';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site GENERAL table!
        $deleteData = array('key_code' => $keyCode);
        $wpdb->delete($tableName, $deleteData);
        // ...Delete the data from the entity table.
    }
}
?>