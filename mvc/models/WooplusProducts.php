<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

require_once(plugin_dir_path(__FILE__) . '../models/WooplusAccessKeys.php');

class WooplusProducts {

    public static function isAccessible($productId) {
        $userId = get_current_user_id();
        $needsLogin = get_post_meta($productId, 'access_needs_login', true);
        $needsLogin = ($needsLogin == 'yes') ? 'yes' : NULL;
        if((!empty($needsLogin)) && (!is_user_logged_in())) {
            return false;
        }
        $requiredKeyCode = get_post_meta($productId, 'access_required_key', true);
        if(!empty($requiredKeyCode)) {
            if(!is_user_logged_in() || empty(WooplusAccessKeys::have($requiredKeyCode, $userId))) {
                return false;
            }
        }

        return true;
    }

    public static function listAccessible() {
        global $wpdb;

        $tableName = $wpdb->prefix.'wc_product_meta_lookup';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site SPECIFIC table!
        // ...This is the standard WooCommerce product table!
        $sqlString
        = "SELECT product_id"
        . " FROM " . $tableName
        ;
        $productIds = $wpdb->get_col($sqlString);

        $accessibleIds = array();
        foreach($productIds as $productId) {
            if(WooplusProducts::isAccessible($productId)) {
                $accessibleIds[] = $productId;
            }
        }

        return $accessibleIds;
    }
}
?>