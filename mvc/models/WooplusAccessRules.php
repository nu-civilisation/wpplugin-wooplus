<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

class WooplusAccessRules {

    public static function exists() {
        global $wpdb;
        
        require_once(ABSPATH.'wp-admin/includes/upgrade.php');
        
        $charsetCollate = $wpdb->get_charset_collate();
        // ...Be sure to obtain the configured charset!
        $tableName = $wpdb->prefix.'wooplus_accessrules';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site SPECIFIC table!
        $sqlString
        = "CREATE TABLE ". $tableName . " ("
        . "code VARCHAR(36) NOT NULL, "
        // ...This is an UUIDv4 having the format "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".
        . "label VARCHAR(255) NOT NULL, "
        // ...The label is the custom taxomomy, for which the access rule applies.
        . "requirement VARCHAR(36) NOT NULL, "
        // ...The requirement must be met to access the content below the HTTP path root.
        // ...This is either "logged-in" or an UUIDv4 having the format "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".
        . "redirect VARCHAR(16) NOT NULL, "
        // ...The redirect is triggered, when the requirement is not met.
        . "PRIMARY KEY  (code), "
        // ...Be sure to have TWO(!) spaces after "PRIMARY KEY"!
        . "KEY index_label (label)"
        // ...The idea behind the indices is to narraw DB-internally the data quickly to a small set of data.
        . ") $charsetCollate;"
        ;
        dbDelta($sqlString);
        // ...Modify the WP-database based on the SQL statement.
    }

    public static function have($accessLabel, $accessRequirement) {
        global $wpdb;

        $tableName = $wpdb->prefix.'wooplus_accessrules';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site SPECIFIC table!
        $sqlString
        = "SELECT code"
        . " FROM " . $tableName
        . " WHERE label = '" . $accessLabel . "'"
        . " AND requirement = '" . $accessRequirement . "'"
        ;
        $result = $wpdb->get_var($sqlString);
        // ...The result is a single scalar value.

        return empty($result) ? false : true;
    }

    public static function list() {
        global $wpdb;

        $tableName = $wpdb->prefix.'wooplus_accessrules';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site SPECIFIC table!
        $sqlString
        = "SELECT code, label, requirement, redirect"
        . " FROM " . $tableName
        . " ORDER BY label ASC,"
        . " requirement ASC"
        ;

        return $wpdb->get_results($sqlString);
    }

    public static function listByLabel($accessLabel) {
        global $wpdb;

        $tableName = $wpdb->prefix.'wooplus_accessrules';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site SPECIFIC table!
        $sqlString
        = "SELECT code, label, requirement, redirect"
        . " FROM " . $tableName
        . " WHERE label = '" . $accessLabel . "'"
        . " ORDER BY requirement ASC"
        ;

        return $wpdb->get_results($sqlString);
    }

    public static function get($accessruleCode) {
        global $wpdb;

        $tableName = $wpdb->prefix.'wooplus_accessrules';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site SPECIFIC table!
        $sqlString
        = "SELECT code, label, requirement, redirect"
        . " FROM " . $tableName
        . " WHERE code = '" . $accessruleCode . "'"
        ;

        return $wpdb->get_row($sqlString);
    }

    public static function add($accessruleCode, $accessLabel, $accessRequirement, $accessRedirect) {
        global $wpdb;

        $tableName = $wpdb->prefix.'wooplus_accessrules';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site SPECIFIC table!
        $insertData = array
        ( 'code'         => $accessruleCode
        , 'label'        => $accessLabel
        , 'requirement'  => $accessRequirement
        , 'redirect'     => $accessRedirect
        );

        $insertFormat = array('%s', '%s', '%s', '%s');
        $wpdb->insert($tableName, $insertData, $insertFormat);
        // ...Inserts the data into the entity table.(
    }

    public static function remove($accessruleCode) {
        global $wpdb;

        $row = WooplusAccessRules::get($accessruleCode);
        if(isset($row->code)) {
            $tableName = $wpdb->prefix.'wooplus_accessrules';
            // ...Be sure to obtain the database prefix to be consistent in naming!
            // ...The entity table is a WordPress blog site SPECIFIC table!
            $deleteData = array('code' => $accessruleCode);
            $wpdb->delete($tableName, $deleteData);
            // ...Delete the data from the entity table.
        }
    }
}
?>