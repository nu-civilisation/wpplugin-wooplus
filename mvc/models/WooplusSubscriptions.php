<?php
/* Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 3, as
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
// Ensure, that the plugin is not called directly:
defined('ABSPATH') or die('Illegal access!');

class WooplusSubscriptions {

    public static function exists() {
        global $wpdb;
        
        require_once(ABSPATH.'wp-admin/includes/upgrade.php');
        
        $charsetCollate = $wpdb->get_charset_collate();
        // ...Be sure to obtain the configured charset!
        $tableName = $wpdb->prefix.'wooplus_subscriptions';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site SPECIFIC table!
        $sqlString
        = "CREATE TABLE " . $tableName . " ("
        . "code VARCHAR(36) NOT NULL, "
        // ...This is an UUIDv4 having the format "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".
        . "product_id INT(20) NOT NULL, "
        // ...This denotes the ID of the corresponding WooCommerce product.
        . "user_id INT(20) NOT NULL, "
        // ...This denotes the WP user_id of the owner of this entity.
        . "period_length INT(3) NOT NULL, "
        // ...This is the duration length of the period in the corresponding unit.
        . "period_unit VARCHAR(6) NOT NULL, "
        // ...This is the subscription unit: "days", "weeks", "months", "years".
        . "periods INT(9) NOT NULL, "
        // ...This is the period count of the subscription.
        . "begin_date VARCHAR(10) NOT NULL, "
        // ...This is a date of the format: "YYYY-MM-DD".
        . "end_date VARCHAR(10) NOT NULL, "
        // ...This is a date of the format: "YYYY-MM-DD".
        . "stage VARCHAR(1) NOT NULL, "
        // ...This can be validated ('o') or invalidated ('x').
        . "PRIMARY KEY  (code), "
        // ...Be sure to have TWO(!) spaces after "PRIMARY KEY"!
        . "KEY index_userid (user_id), "
        . "KEY index_productid (product_id), "
        . "KEY index_enddate (end_date)"
        // ...The idea behind the indices is to narraw DB-internally the data quickly to a small set of data.
        . ") $charsetCollate;"
        ;
        dbDelta($sqlString);
        // ...Modify the WP-database based on the SQL statement.
    }

    public static function have($productId, $userId) {
        global $wpdb;

        $tableName = $wpdb->prefix.'wooplus_subscriptions';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site SPECIFIC table!
        $sqlString
        = "SELECT code"
        . " FROM " . $tableName
        . " WHERE user_id = " . $userId . ""
        . " AND product_id = " . $productId . ""
        . " AND stage = 'o'"
        ;
        $result = $wpdb->get_var($sqlString);
        // ...The result is a single scalar value.

        return empty($result) ? false : true;
    }

    public static function listExpired() {
        global $wpdb;

        $today = gmdate('Y-m-d');
        // ...Formats the current timestamp in the format "YYYY-MM-DD"; these are 10 characters.

        $tableName = $wpdb->prefix.'wooplus_subscriptions';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site SPECIFIC table!
        $sqlString
        = "SELECT code, product_id, user_id, period_length, period_unit, periods, begin_date, end_date, stage"
        . " FROM " . $tableName
        . " WHERE end_date > " . $today
        . " AND stage = 'o'"
        ;

        return $wpdb->get_results($sqlString);
    }

    public static function get($productId, $userId) {
        global $wpdb;

        $tableName = $wpdb->prefix.'wooplus_subscriptions';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site SPECIFIC table!
        $sqlString
        = "SELECT code, product_id, user_id, period_length, period_unit, periods, begin_date, end_date, stage"
        . " FROM " . $tableName
        . " WHERE user_id = " . $userId . ""
        . " AND product_id = '" . $productId. "'"
        . " AND stage = 'o'"
        ;

        return $wpdb->get_row($sqlString);
    }

    public static function add($productId, $userId, $periodLength, $periodUnit, $beginDate) {
        global $wpdb;

        $tableName = $wpdb->prefix.'wooplus_subscriptions';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site SPECIFIC table!
        $row = WooplusSubscriptions::get($productId, $userId);
        if(isset($row->code)) {
            // We have an already existing and active subscription -- prolong it:
            $code    = $row->code;
            $endDate = gmdate('Y-m-d', strtotime($row->end_date . ' +' . $periodLength . ' ' . $periodUnit));
            $periods = $row->periods + 1;

            $updateData = array
            ( 'periods'       => $periods
            , 'end_date'      => $endDate
            );
            $updateWhere = array
            ( 'code'          => $code
            );
            $updateFormat = array('%d', '%s');
            $wpdb->update($tableName, $updateData, $updateWhere, $updateFormat);
            // ...Update the data in the entity table.
        }
        else {
            // We don't have an active subscription, create a new one:
            $endDate = gmdate('Y-m-d', strtotime($beginDate . ' +' . $periodLength . ' ' . $periodUnit));
            $insertData = array
            ( 'code'          => wp_generate_uuid4()
            , 'product_id'    => $productId
            , 'user_id'       => $userId
            , 'period_length' => $periodLength
            , 'period_unit'   => $periodUnit
            , 'periods'       => 1
            , 'begin_date'    => $beginDate
            , 'end_date'      => $endDate
            , 'stage'         => 'o'
            // ...The initial stage is always valiated at creation time.
            );
    
            $insertFormat = array('%s', '%d', '%d', '%d', '%s', '%d', '%s', '%s', '%s');
            $wpdb->insert($tableName, $insertData, $insertFormat);
            // ...Inserts the data into the entity table.(
        }
    }

    public static function remove($productId, $userId) {
        global $wpdb;

        $today = gmdate('Y-m-d');
        // ...Formats the current timestamp in the format "YYYY-MM-DD"; these are 10 characters.

        $row = WooplusSubscriptions::get($productId, $userId);
        if(isset($row->code)) {
            $end           = gmdate('Y-m-d', strtotime($row->end_date . ' -' . $row->period_length . ' ' . $row->period_unit));
            $periods       = $row->periods - 1;
            $todayDateTime = new DateTime($today);
            $endDateTime   = new DateTime($end);
            $endDate       = NULL;
            $stage         = NULL;
            if($endDateTime > $todayDateTime) {
                // The new end date is in the future -- the subscription is still active:
                $endDate = $end;
                $stage   = 'o';
            }
            else {
                // The new end date is in the past -- the subscription has exired:
                $endDate = $today;
                $stage   = 'x';
            }
            $tableName = $wpdb->prefix.'wooplus_subscriptions';
            // ...Be sure to obtain the database prefix to be consistent in naming!
            // ...The entity table is a WordPress blog site SPECIFIC table!
            $updateData = array
            ( 'periods'  => $periods
            , 'end_date' => $endDate
            , 'stage'    => $stage
            // ...Set the stage to invalidated.
            );
            $updateWhere = array
            ( 'code'     => $row->code
            );
            $updateFormat = array('%s', '%s');
            $wpdb->update($tableName, $updateData, $updateWhere, $updateFormat);
            // ...Update the data in the entity table.
        }
    }

    public static function removeExpired() {
        global $wpdb;

        $today = gmdate('Y-m-d');
        // ...Formats the current timestamp in the format "YYYY-MM-DD"; these are 10 characters.
        
        $tableName = $wpdb->prefix.'wooplus_subscriptions';
        // ...Be sure to obtain the database prefix to be consistent in naming!
        // ...The entity table is a WordPress blog site SPECIFIC table!
        $sqlString
        = "UPDATE " . $tableName
        . " SET stage = 'x'"
        . " WHERE end_date < %s"
        . " AND stage = 'o'"
        ;

        $wpdb->query($wpdb->prepare
            ( $sqlString
            , $today
            )
        );
        // ...Update the data in the entity table.
    }
}
?>