# Copyright NU-CIVILISATION – Gemeinschaft zur Förderung einer organischen gemeinwohl- und werteorientierten Zivilisation.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License, version 3, as
# published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


msgid  "New access key was added."
msgstr ""

msgid  "The access key was edited."
msgstr ""

msgid  "An access key was removed."
msgstr ""

msgid  "The access key was translated."
msgstr ""

msgid  "Access Key Name"
msgstr ""

msgid  "Translation"
msgstr ""

msgid  "This Site Only"
msgstr ""

msgid  "Cmd"
msgstr ""

msgid  "OK"
msgstr ""

msgid  "Add"
msgstr ""

msgid  "Edit"
msgstr ""

msgid  "Translate"
msgstr ""

msgid  "Remove"
msgstr ""

msgid  "New page access rule was added."
msgstr ""

msgid  "A page access rule was removed."
msgstr ""

msgid  "Label"
msgstr ""

msgid  "Requirement"
msgstr ""

msgid  "Redirect"
msgstr ""

msgid  "Logged-in"
msgstr ""

msgid  "Page: 404"
msgstr ""

msgid  "Page: Home"
msgstr ""

msgid  "Page: Login"
msgstr ""

msgid  "New access label was added."
msgstr ""

msgid  "An access label was removed."
msgstr ""

msgid  "Access Label Name"
msgstr ""

msgid  "Access Labels"
msgstr ""

msgid  "Needs login"
msgstr ""

msgid  "Required Access-Key"
msgstr ""

msgid  "The required access key specifies the visibility of the product: this access key is needed to show the product."
msgstr ""

msgid  "Subscription"
msgstr ""

msgid  "Determines, if this product is a subscription."
msgstr ""

msgid  "Subscription period length"
msgstr ""

msgid  "The decimal value of the subscription length per period unit."
msgstr ""

msgid  "Subscription period unit"
msgstr ""

msgid  "days"
msgstr ""

msgid  "weeks"
msgstr ""

msgid  "months"
msgstr ""

msgid  "years"
msgstr ""

msgid  "The subscription period unit for the provided subscription length."
msgstr ""

msgid  "Gaining Access Keys"
msgstr ""

msgid  "New Gaining Access-Key"
msgstr ""

msgid  "The gaining access-keys specify which keys are gained with obtaining the product."
msgstr ""

msgid  "Deselecting this checkbox removes the access key."
msgstr ""
